<?php
session_start();
$connection = new PDO('mysql:host=localhost;dbname=khairul', 'root', '');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $permited = array('jpg', 'jpeg', 'png', 'gif');
    $file_name = $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_temp = $_FILES['image']['tmp_name'];
//    echo $file_name;
//    echo $file_size;
//    echo $file_temp;

    $div = explode('.', $file_name);
    $ext = strtolower(end($div));
    $image_unique = substr(md5(time()), 0, 5) . '.' . $ext;

    $uploaded_image = "uploads/$image_unique";
    if (empty($file_name)) {
        $_SESSION['error_msg'] = '<b style="color: red;">Please Select Image</b>';
        header('location:index.php');
    } elseif ($file_size > 1000000) {
        $_SESSION['error_msg'] = '<b style="color: red;">Image file Shuld be Less then 1MB...!</b>';
        header('location:index.php');
    } elseif (in_array($ext, $permited) === FALSE) {
        $_SESSION['error_msg'] = 'You can select only:-' . implode(', ', $permited);
        header('location:index.php');
    } else {
        move_uploaded_file($file_temp, $uploaded_image);
        $query = "INSERT INTO tbl_image(image) VALUES('$uploaded_image')";
        $stmt = $connection->prepare($query);
        $inserted_rows = $stmt->execute();

        if ($inserted_rows) {
            $_SESSION['error_msg']= '<span style="color: red;">Image Inserted Successfully </span>';
            header('location:index.php');
        } else {
            $_SESSION['error_msg']= '<span style="color: red;">Image Not Inserted !</span>';
            header('location:index.php');
        }
    }
}
?>