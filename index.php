
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table{
                margin: 0 auto;
                border: 1px solid #990099;
                padding: 20px;
            }
        </style>
    </head>
    <body>
        <form action="upload_image.php" method="post" enctype="multipart/form-data">
            <table>
                <?php
                session_start();
                if(isset($_SESSION['error_msg'])){ ?>
                <tr>
                    <td style="color: red;">
                        <?php echo $_SESSION['error_msg']; unset($_SESSION['error_msg']);?>
                    </td>
                </tr>
               <?php } ?>
            <tr>
                <td>Select Image</td>
                <td><input type="file" name="image"/></td>
            </tr>
            <tr>
                
                <td><input type="submit" value="Upload"/></td>
            </tr>
        </table>
        </form>
    </body>
</html>
